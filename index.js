/*
 * I've used blessed to create a textbox at the bottom line in the screen.
 * The rest of the screen is the 'body' where your code output will be added.
 * This way, when you type input, your program won't muddle it with output.
 *
 * To try this code:
 * - $ npm install blessed --save
 * - $ node screen.js
 *
 * Key points here are:
 * - Your code should show output using the log function.
 *   Think of this as a console.log drop-in-replacement.
 *   Don't use console.* functions anymore, they'll mess up blessed's screen.
 * - You have to 'focus' the inputBar element for it to receive input.
 *   You can have it always focused, however, but my demonstration shows listening for an enter key press or click on the blue bar to focus it.
 * - If you write code that manipulates the screen, remember to run screen.render() to render your changes.
 */

const blessed = require('blessed');
const dotenv = require("dotenv")
dotenv.config()
const api = require('binance');
async function main() {
    var screen = blessed.screen();
    var body = blessed.box({
    top: 0,
    left: 0,
    height: '100%-1',
    width: '100%',
    keys: true,
    mouse: true,
    alwaysScroll: true,
    scrollable: true,
    scrollbar: {
        ch: ' ',
        bg: 'red'
    }
    });
    var inputBar = blessed.textbox({
    bottom: 0,
    left: 0,
    height: 1,
    width: '100%',
    keys: true,
    mouse: true,
    inputOnFocus: true,
    style: {
        fg: 'white',
        bg: 'blue'	// Blue background so you see this is different from body
    }
    });

    // Add body to blessed screen
    screen.append(body);
    screen.append(inputBar);

    // Close the example on Escape, Q, or Ctrl+C
    screen.key(['escape', 'q', 'C-c'], (ch, key) => (process.exit(0)));
    const binanceRest = new api.BinanceRest({
        key: process.env.API_KEY, // Get this from your account on binance.com
        secret: process.env.API_SECRET, // Same for this
        timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
        recvWindow: 10000, // Optional, defaults to 5000, increase if you're getting timestamp errors
        disableBeautification: false,
        /*
        * Optional, default is false. Binance's API returns objects with lots of one letter keys.  By
        * default those keys will be replaced with more descriptive, longer ones.
        */
        handleDrift: true,
        /*
        * Optional, default is false.  If turned on, the library will attempt to handle any drift of
        * your clock on it's own.  If a request fails due to drift, it'll attempt a fix by requesting
        * binance's server time, calculating the difference with your own clock, and then reattempting
        * the request.
        */
        baseUrl: 'https://api.binance.com/',
        /*
        * Optional, default is 'https://api.binance.com/'. Can be useful in case default url stops working.
        * In february 2018, Binance had a major outage and when service started to be up again, only
        * https://us.binance.com was working.
        */
        requestOptions: {}
        /*
        * Options as supported by the 'request' library
        * For a list of available options, see:
        * https://github.com/request/request
        */
    });



    // Handle submitting data
    inputBar.on('submit', async (text)=> {
        //log(text);
        inputBar.clearValue();
        if(text.includes('panic')){
            text = (text.replace('panic ', '')).toUpperCase()+'BTC'
            await panicSell(text)
        }else if(text.includes('pump')){
            const initialTime = Date.now()//(await binanceRest.time()).serverTime
            // take input market, uppercase, add BTC
            // match market to binance and display a confirmation wiht price
            const symbol = (text.replace('pump ', '')).toUpperCase()+'BTC'
            const market = exchangeInfo.symbols.filter(function (a) { if(a.symbol === symbol){return a} })[0]
            const ticker = await binanceRest.tickerPrice({symbol: market.symbol})
            if(typeof market !== 'undefined' && typeof ticker !== 'undefined'){
                log('✅ - Trading '+market.symbol+', current price is '+ ticker.price)
                if(process.env.SPLIT_BUYS > 1){
                    await multipleMarketBuy(initialTime, market, ticker)
                }else{
                    await singleMarketBuy(initialTime, market, ticker)
                }
            }
        }
        else if(text.includes('stop-loss')){
            const initialTime = Date.now()//(await binanceRest.time()).serverTime
            // take input market, uppercase, add BTC
            // match market to binance and display a confirmation wiht price
            // stop-loss via 5% 50% to set a stop-loss on VIA for 50% of the bag at 5% price
            const input = (text.replace('stop-loss ', '')).split(' ')
            const symbol = input[0].toUpperCase()+'BTC'
            const stopLoss = Number(intput[1].replace('%', ''))
            const BagQty = Number(intput[2].replace('%', ''))
            const market = exchangeInfo.symbols.filter(function (a) { if(a.symbol === symbol){return a} })[0]
            const ticker = await binanceRest.tickerPrice({symbol: market.symbol})
            if(typeof market !== 'undefined' && typeof ticker !== 'undefined'){
                log('✅ - Trading '+market.symbol+', current price is '+ ticker.price)
                    await singleStopLoss(initialTime, market, ticker, stopLoss, BagQty)
            }
        }
    
    });

    // Add text to body (replacement for console.log)
    const log = (text) => {
    body.pushLine(text);
    screen.render();
    }

    // Listen for enter key and focus input then
    screen.key('enter', (ch, key) => {
    inputBar.focus();
    });
    // Log example output
    /* setInterval(() => {
    log("Just log some example output");
    }, 1000); */

    inputBar.focus();
    screen.render();

    const binanceTime = await binanceRest.time()
    if(typeof binanceTime !== 'undefined'){
        // conected
        log('✔️ - Connected to Binance')
    }else{console.log(binanceTime)}

    // initiate binance connection and retreive list of markets with confirmation
    const exchangeInfo = await binanceRest.exchangeInfo()
    if(typeof exchangeInfo !== 'undefined'){
        // conected
        log('✔️ - Retreived Markets Informations')
    }

    // retreive account info
    const accountInfo = await binanceRest.account()
    let btcBalance = 0
    if(typeof accountInfo !== 'undefined'){
        // conected
        log('✔️ - Retreived Account Informations')
        btcBalance = accountInfo.balances.filter(function (a){if(a.asset === 'BTC'){return a}})[0].free
    }

    // how to use
    log('****************')
    log('NASOS Initiated')
    log('****************')
    log("1) Type in 'pump $market' (ex, pump eth) to pump and press enter")
    log("2) If it goes south, type 'panic $market' (ex, panic eth) to cancel sell orders and sell everything at +5%")

    async function singleMarketBuy(initialTime, market, ticker){
        let accountQuantityToTrade = btcBalance * (process.env.BALANCE_PERCENT/100)
        log('✳️ - Spliting into 1 orders' )
        let firstBoughtPrice = 0
        let totalBoughtQuantity = 0

        let quantity = Number(((accountQuantityToTrade/ticker.price) - ((accountQuantityToTrade/ticker.price) % market.filters.filter(function(a){if(a.filterType === "LOT_SIZE"){return a}})[0].stepSize)).toFixed(market.baseAssetPrecision))
        try{
            const order = await binanceRest.newOrder({symbol: market.symbol, side: 'BUY', type: 'MARKET', quantity: quantity })
            if(typeof order !== 'undefined'){
                // console.log(order)
                // weightedAveragePrice
                let avgWPrice = 0
                let totalQty = 0
                for(const fill of order.fills){
                    avgWPrice = Number(avgWPrice + (fill.price * fill.qty))
                    totalQty = Number(totalQty + Number(fill.qty))
                }
                //console.log(avgWPrice, total_qty)
                avgWPrice = Number(Number(avgWPrice / totalQty).toFixed(market.baseAssetPrecision))
                totalBoughtQuantity = Number(totalBoughtQuantity + totalQty)
                let difference = order.transactTime-initialTime
                let timeName = 'ms'
                if(difference > 1000){
                    timeName = 's'
                    difference = difference/1000
                }
                log('✅ - Bought '+order.cummulativeQuoteQty+' '+ market.symbol+' at avg price of '+ avgWPrice +' on ' + Date(order.transactTime).toString().trim(35) + ' ('+difference + timeName +' later)')
                if(firstBoughtPrice === 0){
                    firstBoughtPrice = avgWPrice
                }
            }
        }catch (err){
            console.log(err)
        }
        // buy is over
        try{
            await limitSell(initialTime, totalBoughtQuantity, market, firstBoughtPrice)
        }catch(err){
            log(err)
        }
        
    }

    async function limitSell(initialTime, totalBoughtQuantity, market, firstBoughtPrice){
        log('✳️ - Waiting for funds ...')
        let newBalance = 0
        try{
            while((totalBoughtQuantity > newBalance) && newBalance === 0){
                newBalance = Number((await binanceRest.account()).balances.filter(function (a){if(a.asset === market.symbol.substr(0, (market.symbol.length-3))){return a}})[0].free)
                //console.log(newBalance)
            }
        }catch(err){
            console.log(err)
        }
       
        // funds are accesible
        log('✅ - Funds are available - '+newBalance+ ' '+ market.symbol + ' out off ' + totalBoughtQuantity + ' bought')

        let sellInitialTime = Date.now()
        // calculate sell quantity
        newBalance = Number((newBalance - Number(newBalance % market.filters.filter(function(a){if(a.filterType === "LOT_SIZE"){return a}})[0].stepSize)).toFixed(market.baseAssetPrecision))

        // calculate sell price
        let price = Number((Number(firstBoughtPrice * (1+(process.env.PROFIT_GOAL/100))) - Number(firstBoughtPrice * (1+(process.env.PROFIT_GOAL/100)) % market.filters.filter(function(a){if(a.filterType === "PRICE_FILTER"){return a}})[0].tickSize)).toFixed(market.baseAssetPrecision))
        log('✳️ - Profit Goal is '+process.env.PROFIT_GOAL+'%, posting at '+ price)

        let finalTime = 0
        try{
            const order = await binanceRest.newOrder({symbol: market.symbol, side: 'SELL', type: 'LIMIT', quantity: newBalance, price: price, timeInForce: 'GTC' })
            if(typeof order !== 'undefined'){
                //console.log(order)
                finalTime = order.transactTime
                let difference = order.transactTime-sellInitialTime
                let timeName = difference > 1000 ? 's' : 'ms'
                log('✅ - Sell Order Posted '+order.origQty+' '+ market.symbol+' at price of '+ order.price +' on ' + Date(order.transactTime).toString().trim(35) + ' ('+difference + timeName +' later)')
            }
        }catch (err){
            log(err)
        }

        log('✅ - Check Binance. - Total execution time : ' + ((finalTime - initialTime)/1000).toFixed(3) + 's')
    }

    async function multipleMarketBuy(initialTime, market, ticker){
        let accountQuantityToTrade = btcBalance * (process.env.BALANCE_PERCENT/100)
        let splitBuys = process.env.SPLIT_BUYS
        log('✳️ - Spliting into ' + splitBuys + ' orders' )
        let accountSplittedQuantityToTrade = accountQuantityToTrade/splitBuys
        let initialPrice = ticker.price
        let firstBoughtPrice = 0
        let totalBoughtQuantity = 0
        while(splitBuys > 0){
            // chekc price
            if(process.env.SPLIT_BUYS !== 1){
                ticker = await binanceRest.tickerPrice({symbol: market.symbol})
            }
           
            // buy unless price is more than profittGoal-30%
            if(ticker.price <= initialPrice * (1+((process.env.PROFIT_GOAL - process.env.PROFIT_GOAL_SAFETY )/100))){
                let quantity = Number(((accountSplittedQuantityToTrade/ticker.price) - ((accountSplittedQuantityToTrade/ticker.price) % market.filters.filter(function(a){if(a.filterType === "LOT_SIZE"){return a}})[0].stepSize)).toFixed(market.baseAssetPrecision))
                try{
                    const order = await binanceRest.newOrder({symbol: market.symbol, side: 'BUY', type: 'MARKET', quantity: quantity })
                    if(typeof order !== 'undefined'){
                        // console.log(order)
                        // weightedAveragePrice
                        let avgWPrice = 0
                        let totalQty = 0
                        for(const fill of order.fills){
                            avgWPrice = Number(avgWPrice + (fill.price * fill.qty))
                            totalQty = Number(totalQty + Number(fill.qty))
                        }
                        //console.log(avgWPrice, total_qty)
                        avgWPrice = Number(Number(avgWPrice / totalQty).toFixed(market.baseAssetPrecision))
                        totalBoughtQuantity = Number(totalBoughtQuantity + totalQty)
                        let difference = order.transactTime-initialTime
                        let timeName = 'ms'
                        if(difference > 1000){
                            timeName = 's'
                            difference = difference/1000
                        }
                        log('✅ - Bought '+order.cummulativeQuoteQty+' '+ market.symbol+' at avg price of '+ avgWPrice +' on ' + Date(order.transactTime).toString().trim(35) + ' ('+difference + timeName +' later)')
                        if(firstBoughtPrice === 0){
                            firstBoughtPrice = avgWPrice
                        }
                    }
                }catch (err){
                    console.log(err)
                }
            }
            splitBuys--
        }
        // buy is over
        await limitSell(initialTime, totalBoughtQuantity, market, firstBoughtPrice)
    }

    async function panicSell(symbol){
        // cancel all sell orders
        try{
            const openOrders = (await binanceRest.openOrders({symbol: symbol}))
            //console.log(openOrders)
            for(const order of openOrders){
                await binanceRest.cancelOrder({symbol: symbol, origClientOrderId: order.clientOrderId})
                log('✳️ - Order '+ order.clientOrderId +'is Canceled!')
            }

            const market = exchangeInfo.symbols.filter(function (a) { if(a.symbol === symbol){return a} })[0]
            //get the last buy price
            const trades = (await binanceRest.myTrades({symbol: market.symbol})).filter(function(a){if(a.isBuyer){return a}})
            const lastTrade = trades[trades.length - 1]

            const ticker = await binanceRest.tickerPrice({symbol: market.symbol})
            // sell all at +5%
            let newBalance = 0
            try{
                while(newBalance === 0){
                    newBalance = Number((await binanceRest.account()).balances.filter(function (a){if(a.asset === market.symbol.substr(0, (market.symbol.length-3))){return a}})[0].free)
                    //console.log(newBalance)
                }
            }catch(err){
                console.log(err)
            }
            
            // funds are accesible
            log('✅ - Funds are available - '+newBalance+ ' '+ market.symbol)

            // calculate sell quantity
            newBalance = Number((newBalance - Number(newBalance % market.filters.filter(function(a){if(a.filterType === "LOT_SIZE"){return a}})[0].stepSize)).toFixed(market.baseAssetPrecision))
    
            // calculate sell price
            let price = Number((Number(lastTrade.price * (1+(process.env.PANIC_GOAL/100))) - Number(lastTrade.price * (1+(process.env.PANIC_GOAL/100)) % market.filters.filter(function(a){if(a.filterType === "PRICE_FILTER"){return a}})[0].tickSize)).toFixed(market.baseAssetPrecision))
            
            if(Number(ticker.price) > Number(price)){
                log('✳️ - Profit Goal is '+process.env.PANIC_GOAL+'%, posting at market'+ ticker.price)

                try{
                    const order = await binanceRest.newOrder({symbol: market.symbol, side: 'SELL', type: 'MARKET', quantity: newBalance })
                    if(typeof order !== 'undefined'){
                        //console.log(order)
                        log('✅ - Sell Order Posted '+order.origQty+' '+ market.symbol+' at price of '+ order.price +' on ' + Date(order.transactTime).toString().trim(35))
                    }
                }catch (err){
                    console.log(err, price, newBalance)
                }
            }else{
                log('✳️ - Profit Goal is '+process.env.PANIC_GOAL+'%, posting at '+ price)
 
                try{
                    const order = await binanceRest.newOrder({symbol: market.symbol, side: 'SELL', type: 'LIMIT', quantity: newBalance, price: price, timeInForce: 'GTC' })
                    if(typeof order !== 'undefined'){
                        //console.log(order)
                        log('✅ - Sell Order Posted '+order.origQty+' '+ market.symbol+' at price of '+ order.price +' on ' + Date(order.transactTime).toString().trim(35))
                    }
                }catch (err){
                    console.log(err, price, newBalance)
                }
            }
            log('✅ - Check Binance.')
        }catch(err){
            console.log(err)
        }
    }

    async function singleStopLoss(initialTime, symbol, ticker, stopLoss, BagQty){
        // cancel all sell orders
        try{
            const openOrders = (await binanceRest.openOrders({symbol: symbol}))
            //console.log(openOrders)
            for(const order of openOrders){
                await binanceRest.cancelOrder({symbol: symbol, origClientOrderId: order.clientOrderId})
                log('✳️ - Order '+ order.clientOrderId +'is Canceled!')
            }

            const market = exchangeInfo.symbols.filter(function (a) { if(a.symbol === symbol){return a} })[0]
            //get the last buy price
            const trades = (await binanceRest.myTrades({symbol: market.symbol})).filter(function(a){if(a.isBuyer){return a}})
            const lastTrade = trades[trades.length - 1]

            // sell all at +5%
            let newBalance = 0
            try{
                while(newBalance === 0){
                    newBalance = Number(((await binanceRest.account()).balances.filter(function (a){if(a.asset === market.symbol.substr(0, (market.symbol.length-3))){return a}})[0].free)/100*BagQty)
                    //console.log(newBalance)
                }
            }catch(err){
                console.log(err)
            }
            
            // funds are accesible
            log('✅ - Funds are available - '+newBalance+ ' '+ market.symbol)

            // calculate sell quantity
            newBalance = Number((newBalance - Number(newBalance % market.filters.filter(function(a){if(a.filterType === "LOT_SIZE"){return a}})[0].stepSize)).toFixed(market.baseAssetPrecision))
    
            // calculate sell price
            let price = Number((Number(lastTrade.price * (1+(stopLoss/100))) - Number(lastTrade.price * (1+(stopLoss/100)) % market.filters.filter(function(a){if(a.filterType === "PRICE_FILTER"){return a}})[0].tickSize)).toFixed(market.baseAssetPrecision))
            
            log('✳️ - Stop-Loss Goal is '+stopLoss+'%, posting at '+ ticker.price)

            try{
                const order = await binanceRest.newOrder({symbol: market.symbol, side: 'SELL', type: 'STOP_LOSS', quantity: newBalance, stopPrice: price })
                if(typeof order !== 'undefined'){
                    //console.log(order)
                    log('✅ - Sell Order Posted '+order.origQty+' '+ market.symbol+' at price of '+ order.price +' on ' + Date(order.transactTime).toString().trim(35))
                }
            }catch (err){
                console.log(err, price, newBalance)
            }
            log('✅ - Check Binance.')
        }catch(err){
            console.log(err)
        }
    }
}
main()