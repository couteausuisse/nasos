/*
 * I've used blessed to create a textbox at the bottom line in the screen.
 * The rest of the screen is the 'body' where your code output will be added.
 * This way, when you type input, your program won't muddle it with output.
 *
 * To try this code:
 * - $ npm install blessed --save
 * - $ node screen.js
 *
 * Key points here are:
 * - Your code should show output using the log function.
 *   Think of this as a console.log drop-in-replacement.
 *   Don't use console.* functions anymore, they'll mess up blessed's screen.
 * - You have to 'focus' the inputBar element for it to receive input.
 *   You can have it always focused, however, but my demonstration shows listening for an enter key press or click on the blue bar to focus it.
 * - If you write code that manipulates the screen, remember to run screen.render() to render your changes.
 */

const blessed = require('blessed');
const dotenv = require("dotenv")
dotenv.config()
const api = require('binance');
async function main() {
    var screen = blessed.screen();
    var body = blessed.box({
    top: 0,
    left: 0,
    height: '100%-1',
    width: '100%',
    keys: true,
    mouse: true,
    alwaysScroll: true,
    scrollable: true,
    scrollbar: {
        ch: ' ',
        bg: 'red'
    }
    });
    var inputBar = blessed.textbox({
    bottom: 0,
    left: 0,
    height: 1,
    width: '100%',
    keys: true,
    mouse: true,
    inputOnFocus: true,
    style: {
        fg: 'white',
        bg: 'blue'	// Blue background so you see this is different from body
    }
    });

    // Add body to blessed screen
    screen.append(body);
    screen.append(inputBar);

    // Close the example on Escape, Q, or Ctrl+C
    screen.key(['escape', 'q', 'C-c'], (ch, key) => (process.exit(0)));
    const binanceRest = new api.BinanceRest({
        key: process.env.API_KEY, // Get this from your account on binance.com
        secret: process.env.API_SECRET, // Same for this
        timeout: 15000, // Optional, defaults to 15000, is the request time out in milliseconds
        recvWindow: 10000, // Optional, defaults to 5000, increase if you're getting timestamp errors
        disableBeautification: false,
        /*
        * Optional, default is false. Binance's API returns objects with lots of one letter keys.  By
        * default those keys will be replaced with more descriptive, longer ones.
        */
        handleDrift: true,
        /*
        * Optional, default is false.  If turned on, the library will attempt to handle any drift of
        * your clock on it's own.  If a request fails due to drift, it'll attempt a fix by requesting
        * binance's server time, calculating the difference with your own clock, and then reattempting
        * the request.
        */
        baseUrl: 'https://api.binance.com/',
        /*
        * Optional, default is 'https://api.binance.com/'. Can be useful in case default url stops working.
        * In february 2018, Binance had a major outage and when service started to be up again, only
        * https://us.binance.com was working.
        */
        requestOptions: {}
        /*
        * Options as supported by the 'request' library
        * For a list of available options, see:
        * https://github.com/request/request
        */
    });



    // Handle submitting data
    inputBar.on('submit', async (text)=> {
        //log(text);
        inputBar.clearValue();
        if(text.includes('vol')){
            text = (text.replace('vol ', '')).split(" ")
            await volalilityAnalysis(text[0], text[1], text[2], text[3])
        }
    
    });

    // Add text to body (replacement for console.log)
    const log = (text) => {
    body.pushLine(text);
    screen.render();
    }

    // Listen for enter key and focus input then
    screen.key('enter', (ch, key) => {
    inputBar.focus();
    });
    // Log example output
    /* setInterval(() => {
    log("Just log some example output");
    }, 1000); */

    inputBar.focus();
    screen.render();

    const binanceTime = await binanceRest.time()
    if(typeof binanceTime !== 'undefined'){
        // conected
        log('✔️ - Connected to Binance')
    }else{console.log(binanceTime)}

    // initiate binance connection and retreive list of markets with confirmation
    const exchangeInfo = await binanceRest.exchangeInfo()
    if(typeof exchangeInfo !== 'undefined'){
        // conected
        log('✔️ - Retreived Markets Informations')
    }

    // retreive account info
    const accountInfo = await binanceRest.account()
    let btcBalance = 0
    if(typeof accountInfo !== 'undefined'){
        // conected
        log('✔️ - Retreived Account Informations')
        btcBalance = accountInfo.balances.filter(function (a){if(a.asset === 'BTC'){return a}})[0].free
    }

    // how to use
    log('****************')
    log('NEPO Initiated')
    log('****************')
    //log("1) type 'vol $market $timeframe $candleCount $type' (ex, vol ETHBTC 1h 450 high), to analyse the market further")
    await volalilityAnalysis(process.env.NEPO_MARKET, process.env.NEPO_TIMEFRAME, process.env.NEPO_CANDLECOUNT, process.env.NEPO_TYPE, process.env.NEPO_GOAL)

    async function volalilityAnalysis(symbol, timeframe, candlesCount, type, goal){
        // prompt for what timeframe and how many candles to get
        // maybe a form?
/*         screen.append(prompt);
        screen.render();
        var prompt = blessed.prompt({
            left: 'center',
            top: 'center',
            height: 'shrink',
            width: 'shrink',
            border: 'line',
        });
        
        screen.append(prompt);
        screen.key(['q', 'C-c'], function quit() {
            return process.exit(0);
        });
        
        screen.render();
        
        prompt.input('Search:', 'test', function() {});     */  
        //console.log(symbol, timeframe, candlesCount, type)
        try{
            const klines = await binanceRest.klines({symbol: symbol, interval: timeframe, limit: candlesCount})
            // get the klines requested (most recent last)
            // loop throught them to get each candle volatility
            let volatilityArray = []
            for(const kline of klines){
                if(type === 'open'){
                    volatilityArray.push(Number((kline.close - kline.open)/kline.open)*100)
                }else if (type === 'high'){
                    volatilityArray.push(Number((kline.high - kline.low)/kline.low)*100)
                }
            }
            let originalArray = [...volatilityArray]
            let meanVolatility = [...volatilityArray]
            let avgVolatility = [...volatilityArray]
            // display avg and mean volatility IN %
            const avg = Number((avgVolatility.reduce((previous, current) => current += previous))/avgVolatility.length)
            log('* The '+ symbol+ ' Average Price Volatility is ' + avg.toFixed(3)+'%, in the last '+ candlesCount+' ' + timeframe+ ' candles')

            //IN %
            let sortedArray = meanVolatility.sort((a, b) => a - b);
            let mean = (sortedArray[(sortedArray.length - 1) >> 1] + sortedArray[sortedArray.length >> 1]) / 2
            log('* The '+ symbol+ ' Mean Price Volatility is ' + mean.toFixed(3)+'%, in the last '+ candlesCount+' ' + timeframe+ ' candles')

            // for each candle that the price drop is bigger than the avg or mean, check by how much the (1,3,10) candles has risen
            let rises =[]
            let drops = []
            for (const [i, volatility] of originalArray.entries()){
                //console.log(volatility,( volatility >= goal),( volatility < goal))
                if( Number(volatility) >= Number(goal)){
                    // this candle's volatility is higher or equal than the goal
                    let cumulVol = 0
                    for (let index = (i+1); index <= (i+3); index++) {
                        if (index <= (originalArray.length-1)){
                            let nextVol = originalArray[index]
                            //console.log(index, volatilityArray.length, nextVol)
                            cumulVol = Number(cumulVol + nextVol)
                        }
                    }
                    rises.push({time:new Date(klines[i].openTime), volatility:volatility, avg:Number(cumulVol / 3)})
                }else if( Math.abs(volatility) < Number(goal)){
                    let cumulVol = 0
                    for (let index = (i+1); index <= (i+3); index++) {
                        if (index <= (originalArray.length-1)){
                            let nextVol = originalArray[index]
                            //console.log(index, volatilityArray.length, nextVol)
                            cumulVol = Number(cumulVol + nextVol)
                        }
                    }
                    drops.push({time:new Date(klines[i].openTime), volatility:volatility, avg:Number(cumulVol / 3)})
                }
            }
            log('**')
            let avgRiseArray = [...rises]
            const avgRise = Number((avgRiseArray.reduce((previous, current) => current.avg += previous.avg))/avgRiseArray.length)
            log('* When The '+ symbol+ ' intra-candle volatility > '+goal+'%, the Average Next 3 Candle Volatility is '+avgRise+'%, in the last '+ candlesCount+' ' + timeframe+ ' candles')
            log('* '+rises.length+' ('+(rises.length/candlesCount*100).toFixed(2)+'%) candles, last candle: '+rises[rises.length-1].volatility.toFixed(3)+'%, on '+ rises[rises.length-1].time)
            let originalRises = [...rises]
            sortedArray = originalRises.sort((a, b) => a.volatility - b.volatility);
            log('* 50% of the candles rose more than '+((sortedArray[(sortedArray.length - 1) >> 1].volatility + sortedArray[sortedArray.length >> 1].volatility) / 2).toFixed(2)+'%')
            if(rises.length <= 10){
                for(const [i, ri] of rises.entries()){
                    log('rise #'+i+' '+ri.volatility.toFixed(3)+'%, '+ri.time)
                }
            }
            log('**')
            let avgDropArray = [...drops]
            const avgDrop = Number((avgDropArray.reduce((previous, current) => current.avg += previous.avg))/avgDropArray.length)
            log('* When The '+ symbol+ ' intra-candle volatility < '+goal+'%, the Average Next 3 Candle Volatility is '+avgDrop+'%, in the last '+ candlesCount+' ' + timeframe+ ' candles')
            log('* '+drops.length+' ('+(drops.length/candlesCount*100).toFixed(2)+'%) candles, last candle: '+drops[drops.length-1].volatility.toFixed(3)+'%, on '+ drops[drops.length-1].time)
            sortedArray = drops.sort((a, b) => a - b);
            log('* 50% of the candles dropped more than '+((sortedArray[(sortedArray.length - 1) >> 1].volatility + sortedArray[sortedArray.length >> 1].volatility) / 2).toFixed(2)+'%')
            log('**')
        }catch(err){
            console.log(err)
        }

    }
}
main()